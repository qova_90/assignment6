#include "Sink.h"
#include "SimObj.h"
#include <vector>

Sink::Sink(string name)
{
	_name = name;
}

class Sink::Stats {

public:
	/*
		Purpose: Initialize class members
		Parameters: None
		Return Value: None
	*/
	Stats() {
		_eventsCount = 0;
		_totalFlowtime = 0;
		_totalDelaytime = 0;
		_maxFlowtime = 0;
		_maxDelaytime = 0;
		_sumOfServiceTimes = 0;
	}

	/*
		Purpose: Provide number of entities processed
		Parameters: None
		Return Value: Integer count of entities
	*/
	int GetEventsCount()
	{
		return _eventsCount;
	}

	/*
		Purpose: Provide max flow time
		Parameters: None
		Return Value: Time value of max flow time
	*/
	Time GetMaxFlowtime()
	{
		return _maxFlowtime;
	}

	/*
		Purpose: Provide max delay time
		Parameters: None
		Return Value: Time value of max delay
	*/
	Time GetMaxDelaytime()
	{
		return _maxDelaytime;
	}

	/*
		Purpose: Provide average flow time
		Parameters: None
		Return Value: Time value of Average Flow time
	*/
	Time GetAvgFlowtime()
	{
		return _totalFlowtime / _eventsCount;
	}

	/*
		Purpose: Provide Average delay time
		Parameters: None
		Return Value: Time value of avergae delay
	*/
	Time  GetAvgDelaytime()
	{
		return _totalDelaytime / _eventsCount;
	}

	/*
		Purpose: Calculates and provides server utilization in fraction
		Parameters: None
		Return Value: Fraction of busy server time against simulation time
	*/
	Time  GetServerUtilization()
	{
		//For a single server system, assuming current time is the total time of simulation, we use it to calculate server utilization
		return _sumOfServiceTimes /_endTime;
	}

	/*
		Purpose: Record values for each entity used in calculating simulation statistics
		Parameters: each entities time measures of delay, flow and service
		Return Value: None
	*/
	void AddStats(Time delayTime, Time flowTime, Time serviceTime,string serverName,Time endTime) {
		_endTime = endTime; // Set latest endTime
		_eventsCount++; //Increase event count

		//_sumOfServiceTimes += serviceTime; // add entity service time

		_totalDelaytime += delayTime; // add entity delay time
		_maxDelaytime = delayTime > _maxDelaytime ? delayTime : _maxDelaytime; // Set max delaytime

		_totalFlowtime += flowTime; // add entity flow time
		_maxFlowtime = flowTime > _maxFlowtime ? flowTime : _maxFlowtime; // Set max flowtime

		
		ServerDetails* _curr = nullptr;

		for (int a = 0;a < _serverStats.size();++a) {
			if (_serverStats[a]->serverName == serverName) {
				_curr = _serverStats[a];
			}
		}

		if (_curr != nullptr) {
			//Service details found
			_curr->countOfEntities += 1;
			_curr->sumServiceTimes += serviceTime;
		}
		else {
			//Insert New service detail
			_serverStats.push_back(new ServerDetails{ serverName,1,serviceTime});
		}
	}

	/*
		Purpose: Prints out Simulation Statistics to screen
		Parameters: None
		Return Value: None
	*/
	void GetStats()
	{
		cout << "Number of Events " << GetEventsCount() << endl;
		cout << "Max Delay time " << GetMaxDelaytime() << endl;
		cout << "Max Flow time " << GetMaxFlowtime() << endl;
		cout << "Avg Delay time " << GetAvgDelaytime() << endl;
		cout << "Avg Flow time " << GetAvgFlowtime() << endl;
		cout << "Avg Delay time " << GetAvgDelaytime() << endl;
		cout << "*** Server Utilizations **** "  << endl;
		for (int a = 0; a <_serverStats.size();++a) {
			cout << " ServerName: " << _serverStats[a]->serverName 
				 << " Utilization: "<< _serverStats[a]->sumServiceTimes / _endTime
				 << " Entities Served: "<<_serverStats[a]->countOfEntities <<endl;
		}
		
		cout << "End time " << _endTime << endl;
	}

	Time GetEndTime()
	{
		return _endTime;
	}

private:
	int _eventsCount; // Maintain a count of all events in the simulation
	Time _totalFlowtime; //Maintain a sum of flow time of every entity
	Time _totalDelaytime; //Maintain a sum of  delay time of every entity
	Time _maxFlowtime; // Holds max flow time
	Time _maxDelaytime; // Holds max delay time
	Time _sumOfServiceTimes; // Hold Sum of each entities service times to help calculate server utilization
	Time _endTime; //hold completion time of last event


	struct ServerDetails {
		string serverName;
		int countOfEntities;
		Time sumServiceTimes;
	};
	
	vector<ServerDetails*> _serverStats;
};

void Sink::Depart(Entity* en)
{
	cout << "Entity " << en->GetID() << " has departed the system." << endl;
	Time delayTime = en->GetServiceStartTime() - en->GetArrivalTime(),
		 flowTime = en->GetDepartureTime() - en->GetArrivalTime(),
		 serviceTime = en->GetDepartureTime() - en->GetServiceStartTime(),
	     endTime = en->GetDepartureTime();
	string serverName = en->GetServer();

	_stats.AddStats(delayTime, flowTime,serviceTime,serverName,endTime);
	
}



void Sink::GetStats() {
	_stats.GetStats();
}




Sink::Stats Sink::_stats;	//instantiates the Stats object at the start of the simulation


