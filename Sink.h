#pragma once
#include <string>
#include "Entity.h"
#include <iostream>

using namespace std;

class Sink {
private:
	string _name;
	class Stats; //container for all statistics of the simulation
	static Stats _stats; // storage object for every statics
	Time _endTime; //hold completion time of last event

	struct ServerDetails; // Data structure to hold individual server details
public:
	Sink(string name);
	/*
		Purpose: Manage the exist of completed entities
		Parameters: pointer to entity
		Return Value: None
	*/
	void Depart(Entity* en);

	/*
		Purpose: Provides simulation performance measures
		Parameters: None
		Return Value: None
	*/
	void GetStats();

	/*
		Purpose: Return the Endtime of simulation
		Parameters: None
		Return Value: End  Time in Time units
	*/
	Time GetEndTime();
};
