#pragma once
#include "SimObj.h"
#include "Entity.h"
#include "Sink.h"
#include "Distribution.h"
#include "Vector"

class Queue : public SimObj
{
public:
	Queue(string name,int serverCount, Sink* sink, Distribution* serviceTime);
	/*
		Queue
			Parameters:
				none
			Return value:
				none
			Behavior:
				Constructor for the Queue class based on SimObj
	*/

	void ScheduleArrivalIn(Time deltaTime, Entity *en);

	private:
		Sink* _sink;
		string _name;
		Distribution* _serviceTime;
		int _serverCount;
		
		//Server Class
		class Server;


		//Events
		class ArriveEvent;
		class DepartEvent;
		
		//Event Methods
		void Arrive(Entity *en);
		void Depart(Entity* en);
		
		//Vector of Servers
		vector<Server*> _servers;
};
