#include "SimObj.h"
#include "Queue.h"
#include <iostream>
#include "Source.h"
#include "Sink.h"

using namespace std;

int main()
{	
	int entities = 0,
	    servers = 0;

	cout << "Enter desired Number of entities \n 100 is default" << endl;
	cin >> entities;

	entities = entities >0 ? entities:100;

	cout << "Enter Number of Servers \n 2 is default" << endl;
	cin >> servers;

	servers = servers>0 ? servers:2;


	Sink sink("Sink");
	Queue queue("Queue",servers, &sink, new Triangular(2, 3, 4));
	Source source("Source", &queue, new Triangular(1, 3, 5), entities);
	SimObj::RunSimulation();
	sink.GetStats();

	system("Pause");
}