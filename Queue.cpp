#include "Queue.h"
#include "SimObj.h"
#include <iostream>
using namespace std;




class Queue::Server {
public:

	Server(string name) :_name(name) {
		_lastDepartTime = 0;
	}

	string GetServerName() {
		return _name;
	}

	Time GetLastDepartTime() {
		return _lastDepartTime;
	}

	void SetLastDepartTime(Time time) {
		_lastDepartTime = time;
	}

private:
	string _name;
	Time _lastDepartTime;
};


Queue::Queue(string name, int serverCount, Sink* sink, Distribution* serviceTime)
{
	_sink = sink;
	_serviceTime = serviceTime;
	_name = name;
	_serverCount = serverCount;

	for (int a = 0;a < _serverCount;++a) {
		string s = "server" + to_string(a + 1);
		_servers.push_back(new Server(s));
	}		
}

class Queue::ArriveEvent : public Event
{
public:
	ArriveEvent(Queue *queue, Entity *en)
	{
		_queue = queue;
		_en = en;
	}

	void Execute()
	{
		_queue->Arrive(_en);
	}

private:
	Queue *_queue;
	Entity *_en;
};

class Queue::DepartEvent : public Event {
private:
	Queue* _queue;
	Entity* _en;

public:
	DepartEvent(Queue* queue, Entity* en) {
		_queue = queue;
		_en = en;
	}
	void Execute() {
		_queue->Depart(_en);
	}
};

void Queue::ScheduleArrivalIn(Time deltaTime, Entity * en)
{
	ScheduleEventIn(deltaTime, new ArriveEvent(this, en));
}


void Queue::Arrive(Entity * en)
{
	cout << "Entity " << en->GetID() << " has arrived at " << GetCurrentSimTime() << endl;

	Time leastTime = _servers[0]->GetLastDepartTime();
	
	Server* selectedServer = _servers[0];

	for (int i = 0;i < _servers.size();i++){
		if (_servers[i]->GetLastDepartTime() < leastTime) {
			selectedServer = _servers[i];
			leastTime = _servers[i]->GetLastDepartTime();
		}
	}
	
	Time departTime;
	if (leastTime < GetCurrentSimTime())
	{
		departTime = GetCurrentSimTime() + _serviceTime->GetRV();
		en->SetServiceStartTime(GetCurrentSimTime());
	}
	else
	{
		departTime = leastTime+_serviceTime->GetRV();
		en->SetServiceStartTime(leastTime);
	}
	
	
	en->SetDepartureTime(departTime);
	en->SetServer(selectedServer->GetServerName());
	selectedServer->SetLastDepartTime(departTime);
	ScheduleEventAt(departTime, new DepartEvent(this, en));
}

void Queue::Depart(Entity* en)
{
	cout << "Entity " << en->GetID() << " has departed at " << GetCurrentSimTime() <<" Served by "<< en->GetServer()<< endl;
	_sink->Depart(en); //Pass entity to sink node
}



